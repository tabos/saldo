# Copyright (c) 2020-2022 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

"""
Transaction details
"""

from gi.repository import Adw, Gtk
from saldo.widgets.category_dialog import CategoryDialog
from saldo.backend.transaction_data import TransactionData
from saldo.backend.helper import readable_date
from saldo.backend.helper import money


@Gtk.Template(resource_path="/org/tabos/saldo/ui/transaction_details.ui")
class TransactionDetails(Gtk.Box):
    __gtype_name__ = "TransactionDetails"

    _avatar = Gtk.Template.Child()
    _avatar_edit = Gtk.Template.Child()
    back_button = Gtk.Template.Child()
    _booking_date_label = Gtk.Template.Child()
    _value_label = Gtk.Template.Child()
    _amount_label = Gtk.Template.Child()
    _transaction_type_label = Gtk.Template.Child()

    _name_label = Gtk.Template.Child()
    _iban_label = Gtk.Template.Child()
    _bic_label = Gtk.Template.Child()

    _reference_label = Gtk.Template.Child()
    _creditor_id_label = Gtk.Template.Child()
    _mandate_reference_label = Gtk.Template.Child()
    _end_to_end_reference_label = Gtk.Template.Child()

    def __init__(self, unlocked):
        super().__init__()

        self._unlocked = unlocked
        self._dialog = None

    def set_transaction(self, transaction: TransactionData) -> None:
        self._booking_date_label.set_text(readable_date(transaction.date))
        self._value_label.set_text(readable_date(transaction.entry_date))

        amount = money(transaction.amount)
        self._amount_label.set_text(amount + " " + transaction.currency)
        self._transaction_type_label.set_text(transaction.posting_text)

        self._name_label.set_text(transaction.name)
        self._iban_label.set_text(transaction.iban)
        self._bic_label.set_text(transaction.bic)

        self._reference_label.set_text(transaction.purpose)

        self._creditor_id_label.set_text(transaction.applicant_creditor_id)
        self._mandate_reference_label.set_text(
            transaction.additional_position_reference
        )
        self._end_to_end_reference_label.set_text(transaction.end_to_end_reference)

        self._unlocked._backend.category_set_avatar(self._avatar, transaction.name)

        self._avatar_edit.connect("clicked", self.on_clicked)

    @Gtk.Template.Callback()
    def _on_back_button_clicked(self, _):
        self._unlocked.window.start_lock_timer()
        self._unlocked.subpage_leaflet.navigate(Adw.NavigationDirection.BACK)

    def on_clicked(self, button: Gtk.Widget) -> None:  # pylint: disable=unused-argument
        if self._dialog:
            return

        self._dialog = CategoryDialog(self._unlocked._backend)
        self._dialog.connect("response", self._on_category_response)
        self._dialog.present(self._unlocked.window)

    def _on_category_response(self, dialog: Gtk.Widget, response: str) -> None:
        if response == "set":
            category = dialog.get_selected_category()
            self._unlocked._backend.category_add_mapping(
                self._name_label.get_text(), category
            )
        elif response == "unset":
            self._unlocked._backend.category_remove_mapping(self._name_label.get_text())

        self._unlocked._backend.category_set_avatar(
            self._avatar, self._name_label.get_text()
        )
        self._unlocked.update_transactions()

        self._dialog = None
