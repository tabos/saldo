# Copyright (c) 2020-2022 Jan-Michael Brummer <jan.brummer@tabos.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gio, GLib

from saldo import const

setting = Gio.Settings.new(const.APP_ID)


DARK_THEME = "dark-theme"
LOCK_TIMER = "lock-timer"
SAFE_DAYS = "safe-days"
AUTOMATIC_REFRESH = "automatic-refresh"
WINDOW_SIZE = "window-size"
RUN_IN_BACKGROUND = "run-in-background"
FINGERPRINT_QUICKUNLOCK = "fingerprint-quickunlock"
QUICKUNLOCK = "quickunlock"


def get_dark_theme() -> bool:
    return setting.get_boolean(DARK_THEME)


def set_dark_theme(value: bool) -> None:
    setting.set_boolean(DARK_THEME, value)


def get_lock_timer_seconds() -> int:
    return setting.get_uint(LOCK_TIMER)


def set_lock_timer_seconds(val: int) -> None:
    setting.set_uint(LOCK_TIMER, val)


def get_safe_days() -> int:
    return setting.get_uint(SAFE_DAYS)


def set_safe_days(val: int) -> None:
    setting.set_uint(SAFE_DAYS, val)


def get_automatic_refresh() -> bool:
    return setting.get_boolean(AUTOMATIC_REFRESH)


def set_automatic_refresh(value: bool) -> None:
    setting.set_boolean(AUTOMATIC_REFRESH, value)


def set_window_size(lis):
    g_variant = GLib.Variant("ai", lis)
    setting.set_value(WINDOW_SIZE, g_variant)


def get_window_size():
    return setting.get_value(WINDOW_SIZE)


def get_run_in_background() -> bool:
    return setting.get_boolean(RUN_IN_BACKGROUND)


def set_run_in_background(value: bool) -> None:
    setting.set_boolean(RUN_IN_BACKGROUND, value)


def get_fingerprint_quickunlock():
    return setting.get_boolean(FINGERPRINT_QUICKUNLOCK)


def set_fingerprint_quickunlock(value: bool) -> None:
    setting.set_boolean(FINGERPRINT_QUICKUNLOCK, value)


def get_quickunlock():
    return setting.get_boolean(QUICKUNLOCK)


def set_quickunlock(value: bool) -> None:
    setting.set_boolean(QUICKUNLOCK, value)
