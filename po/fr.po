msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-10-30 14:57+0100\n"
"PO-Revision-Date: 2022-12-12 23:21+0100\n"
"Last-Translator: Irénée Thirion <irenee.thirion@e.email>\n"
"Language-Team: \n"
"Language: fr\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"
"X-Generator: Poedit 3.2.2\n"
"X-Poedit-Basepath: .\n"

#: banking/backend/backend.py:108
msgid "Salary"
msgstr "Salaire"

#: banking/backend/backend.py:109
msgid "Hotel"
msgstr "Hôtel"

#: banking/backend/backend.py:110
msgid "Streaming"
msgstr "Streaming"

#: banking/backend/backend.py:111
msgid "ATM"
msgstr "Distributeur"

#: banking/backend/backend.py:112
msgid "Car"
msgstr "Voitre"

#: banking/backend/backend.py:113
msgid "Fast Food"
msgstr "Fast food"

#: banking/backend/backend.py:114
msgid "Fuel"
msgstr "Essence"

#: banking/backend/backend.py:115
msgid "Games"
msgstr "Jeux"

#: banking/backend/backend.py:116
msgid "Stock"
msgstr "Actions"

#: banking/backend/backend.py:117
msgid "Music"
msgstr "Musique"

#: banking/backend/backend.py:118
msgid "Animal"
msgstr "Animal"

#: banking/backend/backend.py:119
msgid "Technology"
msgstr "Technologie"

#: banking/backend/backend.py:120
msgid "Pharmarcy"
msgstr "Pharmacie"

#: banking/backend/backend.py:121
msgid "Restaurant"
msgstr "Restaurant"

#: banking/backend/backend.py:122
msgid "School"
msgstr "École"

#: banking/backend/backend.py:123
msgid "Shopping"
msgstr "Courses"

#: banking/backend/backend.py:124
msgid "Phone"
msgstr "Téléphone"

#: banking/backend/backend.py:125
msgid "Leisure"
msgstr "Loisirs"

#: banking/backend/backend.py:126
msgid "Television"
msgstr "Télévision"

#: banking/backend/backend.py:127
msgid "Fitness"
msgstr "Sport"

#: banking/backend/backend.py:128
msgid "Home"
msgstr "Maison"

#: banking/backend/backend.py:129
msgid "Barber"
msgstr "Coiffeur"

#: banking/backend/backend.py:130
msgid "Lottery"
msgstr "Loterie"

#: banking/backend/backend.py:216
msgid "Other Expenses"
msgstr "Autres dépenses"

#: banking/backend/backend.py:506
msgid "Error fetching account"
msgstr "Erreur lors de la récupération du compte"

#: banking/backend/backend.py:509
msgid "Close"
msgstr "Fermer"

#: banking/backend/helper.py:34
msgid "Today"
msgstr "Aujourd’hui"

#: banking/backend/helper.py:36
msgid "Yesterday"
msgstr "Hier"

#: banking/backend/helper.py:38
msgid "Open Booking Date:"
msgstr "Jour de réservation ouvert :"

#: banking/widgets/category_dialog.py:25
msgid "No Categories Found"
msgstr "Aucune catégorie trouvée"

#: banking/widgets/locked_page.py:41
msgid "Failed to Unlock Safe"
msgstr "Échec du déverouillage du coffre-fort"

#: banking/widgets/unlocked_page.py:219
msgid "Income"
msgstr "Revenu"

#: banking/window.py:77
msgid "New transaction"
msgstr "Nouvelle transaction"

#: banking/window.py:80
#, python-format
msgid "You paid %.2f %s to %s (%s)."
msgstr "Vous avez payé %.2f %s à %s (%s)."

#: banking/window.py:85
msgid "Attention: "
msgstr "Attention : "

#: banking/window.py:88
#, python-format
msgid "You have received %.2f %s from %s (%s)."
msgstr "Vous avez reçu %.2f %s de %s (%s)."

#: banking/window.py:233
msgid "Safe locked due to inactivity"
msgstr "Le coffre-fort a été verrouillé par inactivité"

#: data/ui/about_dialog.ui.in:6
msgid "FinTS online banking."
msgstr "Services bancaires en ligne FinTS."

#: data/ui/category_dialog.ui:8
msgid "Category"
msgstr "Catégorie"

#: data/ui/category_dialog.ui:12
msgid "Unset"
msgstr "Annuler"

#: data/ui/category_dialog.ui:17
msgid "Set"
msgstr "Définir"

#: data/ui/change_password_dialog.ui:6
msgid "Safe Password"
msgstr "Mot de passe du coffre-fort"

#: data/ui/change_password_dialog.ui:10
msgid "Change"
msgstr "Changer"

#: data/ui/change_password_dialog.ui:41
msgid "Current Password"
msgstr "Mot de passe actuel"

#: data/ui/change_password_dialog.ui:52
msgid "New Password"
msgstr "Nouveau mot de passe"

#: data/ui/change_password_dialog.ui:63
msgid "Confirm Password"
msgstr "Confirmer le mot de passe"

#: data/ui/clients.ui:7
msgid "Add Client"
msgstr "Ajouter un compte client"

#: data/ui/clients.ui:11
msgid "Next"
msgstr "Suivant"

#: data/ui/clients.ui:42
msgid "Search for name, BIC or BLZ"
msgstr "Rechercher un nom, un code BIC ou BLZ"

#: data/ui/clients.ui:73
msgid "You have selected"
msgstr "Vous avez sélectionné"

#: data/ui/clients.ui:76 data/ui/clients.ui:87
msgid "Name"
msgstr "Nom"

#: data/ui/clients.ui:83
msgid "Credentials"
msgstr "Informations d’authentification"

#: data/ui/clients.ui:84
msgid "Enter bank online login data"
msgstr "Entrez les données de connexion de la banque en ligne"

#: data/ui/clients.ui:93 data/ui/create_safe_page.ui:56
#: data/ui/locked_page.ui:55
msgid "Password"
msgstr "Mot de passe"

#. "Banking" is the application name, do not translate
#: data/ui/create_safe_page.ui:6 data/ui/locked_page.ui:6
#: data/ui/unlocked_page.ui:40 data/ui/welcome_page.ui:6
msgid "_About Banking"
msgstr "À _propos de Banking"

#: data/ui/create_safe_page.ui:41
msgid "Set a Safe Password"
msgstr "Définissez un mot de passe pour le coffre-fort"

#: data/ui/create_safe_page.ui:42
msgid "Be careful not to loose your password."
msgstr "Veillez à ne pas perdre votre mot de passe."

#: data/ui/create_safe_page.ui:65
msgid "Confirm"
msgstr "Confirmer"

#: data/ui/create_safe_page.ui:77
msgid "Create"
msgstr "Créer"

#: data/ui/help-overlay.ui:11
msgctxt "Shortcut window description"
msgid "Application"
msgstr "Application"

#: data/ui/help-overlay.ui:14
msgctxt "Shortcut window description"
msgid "Open Preferences"
msgstr "Ouvrir les Préférences"

#: data/ui/help-overlay.ui:20
msgctxt "shortcut window"
msgid "Show Shortcuts"
msgstr "Afficher les raccourcis"

#: data/ui/help-overlay.ui:26
msgctxt "Shortcut window description"
msgid "Close Window"
msgstr "Fermer la fenêtre"

#: data/ui/help-overlay.ui:32
msgctxt "Shortcut window description"
msgid "Quit"
msgstr "Quitter"

#: data/ui/help-overlay.ui:40
msgctxt "Shortcut window description"
msgid "Safe"
msgstr "Coffre-fort"

#: data/ui/help-overlay.ui:43
msgctxt "Shortcut window description"
msgid "Lock Safe"
msgstr "Verrouiller le coffre-fort"

#: data/ui/help-overlay.ui:49
msgctxt "Shortcut window description"
msgid "Refresh Safe"
msgstr "Actualiser le coffre-fort"

#: data/ui/locked_page.ui:42
msgid "Safe is Locked"
msgstr "Coffre-fort verrouillé"

#: data/ui/locked_page.ui:43
msgid "Enter password to unlock"
msgstr "Saisissez le mot de passe pour le déverrouiller"

#: data/ui/locked_page.ui:63
msgid "Unlock"
msgstr "Déverrouiller"

#: data/ui/mechanism_dialog.ui:10
msgid "Select"
msgstr "Sélectionner"

#: data/ui/mechanism_dialog.ui:44
msgid "Choose TAN Mechanism"
msgstr "Choisissez un mécanisme de Numéro d’Authentification de Transaction"

#: data/ui/settings_dialog.ui:9 data/ui/settings_dialog.ui:12
#: data/ui/transfer.ui:37
msgid "General"
msgstr "Général"

#: data/ui/settings_dialog.ui:16
msgid "_Dark Theme"
msgstr "Thème _sombre"

#: data/ui/settings_dialog.ui:17
msgid "Use dark GTK theme."
msgstr "Utiliser le thème sombre GTK."

#: data/ui/settings_dialog.ui:32
msgid "Safe"
msgstr "Coffre-fort"

#: data/ui/settings_dialog.ui:35
msgid "_Lock Timeout"
msgstr "_Minuterie de verrouillage automatique"

#: data/ui/settings_dialog.ui:36
msgid "Lock safe after n seconds."
msgstr "Verrouiller le coffre-fort après n secondes."

#: data/ui/settings_dialog.ui:58 data/ui/window.ui:4
#: data/org.tabos.banking.desktop.in.in:3
#: data/org.tabos.banking.appdata.xml.in.in:6
msgid "Banking"
msgstr "Banking"

#: data/ui/settings_dialog.ui:61
msgid "_Sales days"
msgstr "Jours de _solde"

#: data/ui/settings_dialog.ui:62
msgid "Number of sales days to load."
msgstr "Nombre de jours de solde à charger."

#: data/ui/settings_dialog.ui:82
msgid "_Automatic Refresh"
msgstr "_Actualisation automatique"

#: data/ui/settings_dialog.ui:83
msgid "Update bank transactions every 15 minutes."
msgstr "Mettre à jour les transactions bancaires toutes les 15 minutes."

#: data/ui/tan_dialog.ui:8
msgid "TAN is required"
msgstr "Numéro d’Authentification de Transaction requis"

#: data/ui/tan_dialog.ui:12
msgid "Submit"
msgstr "Soumettre"

#: data/ui/tan_dialog.ui:46
msgid "Enter TAN"
msgstr "Entrez le Numéro d’Authentification de Transaction"

#: data/ui/transaction_details.ui:11 data/ui/transfer.ui:80
msgid "Details"
msgstr "Détails"

#: data/ui/transaction_details.ui:17
msgid "Back"
msgstr "Retour"

#: data/ui/transaction_details.ui:52
msgid "Avatar"
msgstr "Avatar"

#: data/ui/transaction_details.ui:67
msgid "Amount"
msgstr "Montant"

#: data/ui/transaction_details.ui:77
msgid "Booking date"
msgstr "Jour de réservation"

#: data/ui/transaction_details.ui:87
msgid "Reference"
msgstr "Référence"

#: data/ui/transaction_details.ui:98
msgid "Transaction type"
msgstr "Type de transaction"

#: data/ui/transaction_details.ui:112
msgid "Value"
msgstr "Valeur"

#: data/ui/transaction_details.ui:122
msgid "IBAN"
msgstr "IBAN"

#: data/ui/transaction_details.ui:132
msgid "BIC"
msgstr "BIC"

#: data/ui/transaction_details.ui:142
msgid "Creditor ID"
msgstr "ID du créancier"

#: data/ui/transaction_details.ui:152
msgid "Mandate reference"
msgstr "Référence du mandat"

#: data/ui/transaction_details.ui:162
msgid "End-to-end reference"
msgstr "Référence de bout en bout"

#: data/ui/transfer.ui:6
msgid "Transfer"
msgstr "Transférer"

#: data/ui/transfer.ui:10
msgid "Send"
msgstr "Envoyer"

#: data/ui/transfer.ui:40
msgid "_Account"
msgstr "_Compte"

#: data/ui/transfer.ui:52
msgid "Recipient"
msgstr "Récipiendaire"

#: data/ui/transfer.ui:55
msgid "_Name"
msgstr "_Nom"

#: data/ui/transfer.ui:61
msgid "_IBAN"
msgstr "_IBAN"

#: data/ui/transfer.ui:67
msgid "_Bank"
msgstr "_Banque"

#: data/ui/transfer.ui:83
msgid "A_mount"
msgstr "_Montant"

#: data/ui/transfer.ui:90
msgid "_Reference"
msgstr "_Référence"

#: data/ui/unlocked_page.ui:6
msgid "_Lock Safe"
msgstr "_Verrouiller le coffre-fort"

#: data/ui/unlocked_page.ui:10
msgid "_Transfer"
msgstr "_Transférer"

#: data/ui/unlocked_page.ui:14
msgid "_Change Password…"
msgstr "_Modifier le mot de passe…"

#: data/ui/unlocked_page.ui:20
msgid "Run in _Background"
msgstr "Exécution en arrière-plan"

#: data/ui/unlocked_page.ui:26
msgid "_Add Client…"
msgstr "_Ajouter un compte client…"

#: data/ui/unlocked_page.ui:30
msgid "_Settings"
msgstr "_Paramètres"

#: data/ui/unlocked_page.ui:34
msgid "_Keyboard Shortcuts"
msgstr "_Raccourcis clavier"

#: data/ui/unlocked_page.ui:63
msgid "Accounts"
msgstr "Comptes"

#: data/ui/unlocked_page.ui:227
msgid "_Transactions"
msgstr "_Transactions"

#: data/ui/unlocked_page.ui:265
msgid "_Statistic"
msgstr "_Statistiques"

#: data/ui/welcome_page.ui:42
msgid "Welcome to Banking"
msgstr "Bienvenue dans Banking"

#: data/ui/welcome_page.ui:43
msgid "Online banking made easy."
msgstr "La banque en ligne en toute simplicité."

#: data/ui/welcome_page.ui:46
msgid "Create Safe"
msgstr "Créer le coffre-fort"

#: data/org.tabos.banking.desktop.in.in:4
msgid "Online Banking"
msgstr "Banque en ligne"

#: data/org.tabos.banking.desktop.in.in:5
msgid "FinTS online banking application"
msgstr "Application de banque en ligne FinTS"

#: data/org.tabos.banking.desktop.in.in:11
msgid "Network;Online;Banking;"
msgstr "Réseau;en ligne;banque;compte;bancaire;"

#: data/org.tabos.banking.appdata.xml.in.in:7
msgid "Keep track of your finance with FinTS online banking"
msgstr "Suivez vos finances avec la banque en ligne FinTS"

#: data/org.tabos.banking.appdata.xml.in.in:9
msgid ""
"An easy way to access your online banking information. Show your balance and "
"transaction based on FinTS online banking information."
msgstr ""
"Un moyen facile d’accéder à vos informations bancaires en ligne. Affichez "
"votre solde et vos transactions sur la base des informations bancaires en "
"ligne FinTS."

#: data/org.tabos.banking.appdata.xml.in.in:17
msgid "Lock screen"
msgstr "Écran de verrouillage"

#: data/org.tabos.banking.appdata.xml.in.in:21
msgid "Main view"
msgstr "Vue principale"

#: data/org.tabos.banking.appdata.xml.in.in:25
msgid "Details view"
msgstr "Vue des détails"

#: data/org.tabos.banking.appdata.xml.in.in:29
msgid "Search view"
msgstr "Vue de la recherche"

#: data/org.tabos.banking.appdata.xml.in.in:33
msgid "Setup assistant"
msgstr "Assisant de configuration"

#: data/org.tabos.banking.appdata.xml.in.in:35
msgid "Jan-Michael Brummer"
msgstr "Jan-Michael Brummer"

#: data/org.tabos.banking.gschema.xml:6
msgid "BLZ"
msgstr "BLZ"

#: data/org.tabos.banking.gschema.xml:7
msgid "BLZ."
msgstr "BLZ."

#: data/org.tabos.banking.gschema.xml:11
msgid "User"
msgstr "Utilisateur"

#: data/org.tabos.banking.gschema.xml:12
msgid "User name which is used to login to online banking."
msgstr "Nom d’utilisateur utilisé pour se connecter à la banque en ligne."

#: data/org.tabos.banking.gschema.xml:16
msgid "Online banking server"
msgstr "Serveur de banque en ligne"

#: data/org.tabos.banking.gschema.xml:17
msgid "FINTS online banking server url."
msgstr "URL du serveur de banque en ligne FINTS."

#: data/org.tabos.banking.gschema.xml:21
msgid "Safe password hash"
msgstr "Hachage du mot de passe du coffre-fort"

#: data/org.tabos.banking.gschema.xml:22
msgid "Hash of the user safe password."
msgstr "Hachage du mot de passe du coffre-fort de l’utilisateur."

#: data/org.tabos.banking.gschema.xml:26
msgid "Use dark GTK theme"
msgstr "Utiliser le thème sombre GTK"

#: data/org.tabos.banking.gschema.xml:27
msgid ""
"Use the dark variant of your GTK+ theme. Please note that not every GTK+ "
"theme has a dark variant."
msgstr ""
"Utiliser la variante sombre du thème GTK+. Notez que tous les thèmes GTK+ ne "
"disposent pas d’une variante sombre."

#: data/org.tabos.banking.gschema.xml:31
msgid "Number of seconds until safe is locked"
msgstr "Nombre de secondes avant que le coffre ne soit verrouillé"

#: data/org.tabos.banking.gschema.xml:32
msgid "Safe will be locked automatically after n seconds of inactivity."
msgstr ""
"Le coffre-fort sera verrouillé automatiquement après n secondes d’inactivité."

#: data/org.tabos.banking.gschema.xml:36
msgid "Number of days to load"
msgstr "Nombre de jours à charger"

#: data/org.tabos.banking.gschema.xml:37
msgid "Banking will try to load those days."
msgstr "Banking essaiera de charger ces jours."

#: data/org.tabos.banking.gschema.xml:41
msgid "Update safe automatically"
msgstr "Mettre à jour le coffre-fort automatiquement"

#: data/org.tabos.banking.gschema.xml:42
msgid "Whether to refresh accounts automatically."
msgstr "Indique s’il faut actualiser les comptes automatiquement."

#: data/org.tabos.banking.gschema.xml:46
msgid "Window size"
msgstr "Taille de la fenêtre"

#: data/org.tabos.banking.gschema.xml:47
msgid "Remember the window size."
msgstr "Se souvenir de la taille de la fenêtre."

#: data/org.tabos.banking.gschema.xml:51
msgid "Run in background"
msgstr "Exécuter en arrière-plan"

#: data/org.tabos.banking.gschema.xml:52
msgid ""
"If enabled, application continues running in the background after closing "
"the window."
msgstr ""
"Si cette option est activée, l’application continuera de s’exécuter en "
"arrière-plan après la fermeture de la fenêtre."

msgid "A FinTS online banking application for GNOME."
msgstr "Une application bancaire en ligne FinTS pour GNOME."

msgid "Visit Banking website"
msgstr "Visiter la page web de Banking"

msgid "Clients"
msgstr "Clients"

msgid "Bank Code"
msgstr "Code de la banque"

msgid "BIC or BLZ"
msgstr "BIC ou BLZ"

msgid "Bank Information"
msgstr "Informations bancaires"

msgid "Server"
msgstr "Serveur"

msgid "Bank code transit number."
msgstr "Code bancaire numéro de transit."

msgid "_User"
msgstr "_Utilisateur"

msgid "Login user for online banking."
msgstr "Nom de connexion pour la banque en ligne."

msgid "Login password for online banking."
msgstr "Mot de passe de connexion pour la banque en ligne."

msgid "_Clients"
msgstr "_Clients"

msgid "Run in background option"
msgstr "Option pour l’exécution en arrière-plan"

msgid "New application logo"
msgstr "Nouveau logo pour l’application"

msgid "In-App notification"
msgstr "Notification dans l’application"

msgid "Category support"
msgstr "Prise en charge des catégories"

msgid "You have paid %.2f %s to %s."
msgstr "Vous avez payé %.2f %s à %s."
