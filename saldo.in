#!/usr/bin/env python3
# saldo.in
#
# Copyright 2020 Jan-Michael Brummer
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import gettext
import locale
import os
import signal
import sys

_LOCAL = @local_build@

if _LOCAL:
    # In the local use case, use saldo module from the sourcetree
    sys.path.insert(1, '@pythondir@')

    # In the local use case the installed schemas go in <builddir>/data
    os.environ["XDG_DATA_DIRS"] = '@schemasdir@:' + os.environ.get("XDG_DATA_DIRS", "")

import gi

gi.require_version('Adw', '1')
gi.require_version('Gtk', '4.0')
from gi.repository import Gio, Gtk, Adw

PKGDATA_DIR = '@pkgdatadir@'
LOCALE_DIR = '@localedir@'

def set_internationalization():
    """Sets application internationalization."""
    try:
        locale.bindtextdomain('@application_id@', LOCALE_DIR)
        locale.textdomain('@application_id@')
    except AttributeError as e:
        print(
            "Could not bind the gettext translation domain. Some"
            " translations will not work. Error:\n{}".format(e))

    gettext.bindtextdomain('@application_id@', LOCALE_DIR)
    gettext.textdomain('@application_id@')

def set_resources():
    """Sets application resource file."""
    resource = Gio.resource_load(
        os.path.join(PKGDATA_DIR, '@application_id@.gresource'))
    Gio.Resource._register(resource)

def run_application():
    """Runs Saldo application and returns its exit code."""
    from saldo.application import Application

    app = Application('@application_id@')
    signal.signal(signal.SIGINT, signal.SIG_DFL)
    return app.run(sys.argv)

def main():
    """Sets environment and runs Saldo."""
    set_internationalization()
    set_resources()

    Adw.init()

    return run_application()


if __name__ == '__main__':
    if _LOCAL:
        print('Running from source tree, using local files.')
    sys.exit(main())
